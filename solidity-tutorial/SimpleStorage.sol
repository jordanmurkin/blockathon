pragma solidity ^0.4.19;

contract SimpleStorage {
    uint public storedData;
    address public owner;
    
    event OwnerChanged(address indexed owner);
    
    modifier onlyOwner {
        require (msg.sender == owner);
        _;
    }
    
    function SimpleStorage(uint initial) public {
        owner = msg.sender;
        storedData = initial;
    }
    
    function set(uint x) external onlyOwner {
        storedData = x;
    }
    
    function changeOwner(address newOwner) external onlyOwner {
        owner = newOwner;
        OwnerChanged(newOwner);
    }
}